# Appel du module principal

module "principal" {
    source = "./modules"
    external_network = "external"
    router_name = "router_tf_fr"
    network_name = "network_tf_fr"
    subnet_name = "subnet_tf_fr"
    subnet_ip_range = "192.168.61.0/24"
    group_bastion = "secgroup_bastion"
    secgroup_app = "secgroup_application"
    name_instance_bastion = "bastion"
    key_pair = "demo" # nom de votre clé
    key_pair_2 = "key" # nom seconde clé
    secgroup_internal = "secgroup_internal"
    dns = ["192.44.75.10"]
    path_key_pair = "/home/user/Téléchargements/demo.pem" # emplacement de votre clé privée sur la machine hébergeant terraform
    path_key_pair_2 = "/home/ubuntu/.ssh" # emplacement distant où copié  la seconde clé
    path_key_pair_2_local = "/home/user/.ssh" # copie de la seconde clé sur la machine hébergeant terraform
}
