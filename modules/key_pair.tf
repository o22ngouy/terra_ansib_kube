###
# Generate a Public/Private Key Pair of cluster
###

resource "openstack_compute_keypair_v2" "keypair_app" {
  name = var.key_pair_2
}
# connexion à la machine bastion
resource "null_resource" "copy" {
    connection {
        type = "ssh"
        user = "ubuntu"
        private_key = file("${var.path_key_pair}")
        host  =  "${openstack_networking_floatingip_v2.admin.address}"
    }
## copie de la clé sur la machine distante
    provisioner "remote-exec" {
    inline = [
      "echo '${openstack_compute_keypair_v2.keypair_app.public_key}' > ${var.path_key_pair_2}/${var.key_pair_2}.pub",
      "echo '${openstack_compute_keypair_v2.keypair_app.private_key}' > ${var.path_key_pair_2}/${var.key_pair_2}.pem",
      "chmod 600 ${var.path_key_pair_2}/${var.key_pair_2}.pub",
      "chmod 600 ${var.path_key_pair_2}/${var.key_pair_2}.pem"]
    }
# copie de la clé sur la machine hébergeant terraform
    provisioner "local-exec" {
      command = "echo '${openstack_compute_keypair_v2.keypair_app.private_key}' > ${var.path_key_pair_2_local}/${var.key_pair_2}.pem"
    }


    depends_on = [
        openstack_compute_keypair_v2.keypair_app,
        openstack_compute_floatingip_associate_v2.admin
    ]

}

